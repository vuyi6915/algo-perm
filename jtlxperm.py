##################
#Card Questions
##################

# Given a list of letters ['a','a','b','b'], all permutations lexicographically.
# 	Start with aabb

# Walk through the algorithm for n=4.

##################
#Part A
##################
# ALGORITHM LexicographicPermute(n)

# //Generates permutations in lexicographic order
# //Input: A positive integer n
# //Output: A list of all permutations of {1, . . . , n} in lexicographic order

# 	initialize the first permutation with 1,2 . . . n
# 	while last permutation has two consecutive elements in increasing order do
# 		let i be its largest index such that a i < a i+1 //a i+1 > a i+2 > . . . > a n
# 		find the largest index j such that a i < a j //j ≥ i + 1 since a i < a i+1
# 		swap a i with a j //a i+1 a i+2 . . . a n will remain in decreasing order
# 		reverse the order of the elements from a i+1 to a n inclusive
# 		add the new permutation to the list

def permute(n):
	index_guard = len(n) - 1


##################
#Part B
##################

# ALGORITHM
# BRGC(n)
# //Generates recursively the binary reflected Gray code of order n
# //Input: A positive integer n
# //Output: A list of all bit strings of length n composing the Gray code
# 	if n = 1 make list L containing bit strings 0 and 1 in this order
# 	else generate list L1 of bit strings of size n − 1 by calling BRGC(n − 1)
# 	copy list L1 to list L2 in reversed order
# 	add 0 in front of each bit string in list L1
# 	add 1 in front of each bit string in list L2
# 	append L2 to L1 to get list L
# 	return L

def permuteList(mylist):


def testCase(testNumber, inputList,expectedResult):
	actualResult = permuteList(inputList)
	if actualResult == expectedResult: print('Test  ',testNumber,' passed.')
	else: print('Test  ',testNumber,' failed.')

def test():
	testCase(1,['a','a','b','b'], ['aabb','abab','abba','baab','baba','bbaa'])

# A transcript showing your test results goes here